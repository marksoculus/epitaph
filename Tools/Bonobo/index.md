---
layout: default
title: Bonobo
parent: Tools
has_children: true
has_toc: false
nav_order: 7
---
# Bonobo

Bonobo extracts animations from halo games and decompiles them.

Accessing extracted animations requires an edition of 3DS Max from 2013 to current.

#### Compatible with:

* Halo 2 (XBOX)
* Halo 3 (X360)
* Halo 3 ODST (X360)
* Halo Reach (X360)

### [Download](http://forum.halomaps.org/index.cfm?page=topic&topicID=51196)
