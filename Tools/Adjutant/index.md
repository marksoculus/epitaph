---
layout: default
title: Adjutant
parent: Tools
has_children: true
has_toc: false
nav_order: 6
---
# Adjutant

Adjutant extracts various assets from many halo games. Designed in a closed-source format, it allows resource modification of models, sounds, textures, and more.

Accessing extracted models requires an edition of 3DS Max from 2013 to current.

#### Compatible with:

* Halo CE (XBOX, PC)
* Halo CEA (X360)
* Halo 2 (XBOX)
* Halo 3 (X360)
* Halo 3 ODST (X360)
* Halo Reach (X360)
* Halo 4 (X360)

### [Download](http://forum.halomaps.org/index.cfm?page=topic&topicID=45590)
