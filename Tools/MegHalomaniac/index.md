---
layout: default
title: MegHalomaniac
parent: Tools
has_children: true
nav_order: 4
has_toc: false
---
# MegHalomaniac

A game variant disassembler and reassembler. Halo game variant files are in hexadecimal format stored as .bin files. Files are decoded into XML.

Supports Halo Reach and Halo 4.

### [Download](https://github.com/KornnerStudios/KSoft.Blam/releases)

### [Github](https://github.com/KornnerStudios/KSoft.Blam)
