---
layout: default
title: Opus
parent: Tools
has_children: true
nav_order: 3
has_toc: false
---
# Opus
Launch directly into forge and theater maps with the reverse engineering framework Opus. It communicates with the game engine related to each Halo game. This bypasses the default executable for Halo: The Master Chief Collection. It enables more efficient modding practices than DLL injection through exposing features such as direct access to runtime code and memory including input and rendering.

## Project contributors
- [Squaresome](https://github.com/HaydnTrigg)
- [Twister](https://github.com/theTwist84)

## Instructions
1. Download Opus.bin and Opus.exe from the AppVeyor artifacts
2. Insert Opus.bin and Opus.exe in the MCC root directory with mcclauncher.exe.
3. Run Opus.exe with Steam running



### [Download](https://ci.appveyor.com/project/Assault-on-the-Control-Room/opus/build/artifacts)

### [Discord](https://discord.gg/ksvhEQD)

### [Github](https://github.com/Assault-on-the-Control-Room/Opus/)
