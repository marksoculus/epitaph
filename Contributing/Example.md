---
layout: default
title: Example Article
parent: Contributing
has_children: true
nav_order: 5
has_toc: false
---
# Example Article Topic
{: .no_toc}

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## Table of contents (custom)
{: .no_toc .text-delta }
1. [Poking](docs/Tools/Assembly/Poking/)
2. Another list item
3. etc. etc. etc.
{: .custom_toc}
## A Sub-topic
{: .no_toc}

A sub-topic using two hashtags. A main-topic in this location can use one hashtag instead of two.

## Another Sub-topic

Amet risus nullam eget felis eget nunc. Nec ullamcorper sit amet risus nullam eget felis. Nibh tortor id aliquet lectus. Amet luctus venenatis lectus magna fringilla urna porttitor rhoncus. Sapien et ligula ullamcorper malesuada proin libero nunc consequat. Sed velit dignissim sodales ut eu. Suspendisse faucibus interdum posuere lorem. Mauris ultrices eros in cursus turpis massa tincidunt.

## A Sub-topic with blockquote

Amet risus nullam eget felis eget nunc. Nec ullamcorper sit amet risus nullam eget felis:
> Nibh tortor id aliquet lectus. Amet luctus venenatis lectus magna fringilla urna porttitor rhoncus. Sapien et ligula ullamcorper malesuada proin libero nunc consequat. Sed velit dignissim sodales ut eu. Suspendisse faucibus interdum posuere lorem. Mauris ultrices eros in cursus turpis massa tincidunt.
> - [Lorem Ipsum generator](loremipsum.io)

Mauris ultrices eros in cursus turpis massa tincidunt.
